export class Storage{
    constructor(){

    }

    setBlocklist(blockedSites){
        chrome.storage.sync.set({"sites": blockedSites}, function(){
            console.log("Blocked sites have been set");
        });
    }

    getBlocklist(){
        chrome.storage.sync.get(['sites'], function(sites){
            return sites;
        });
    }
}